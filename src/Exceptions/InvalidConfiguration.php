<?php

namespace Cuatrokb\AdminTranslations\Exceptions;

use Cuatrokb\AdminTranslations\Translation;
use Exception;

class InvalidConfiguration extends Exception
{
    public static function invalidModel(string $className): self
    {
        return new static("You have configured an invalid class `{$className}`.".
            'A valid class extends '.Translation::class.'.');
    }
}
